

# YEAST LANDSCAPE MULTIPLEX NETWORK

###### Last update: 1 July 2014

### Reference and Acknowledgments

This README file accompanies the dataset representing the multiplex genetic interaction network of the Saccharomyces Cerevisiae, a species of yeast. 
If you use this dataset in your work either for analysis or for visualization, you should acknowledge/cite the following papers:

	“The Genetic Landscape of a Cell”
	M. Costanzo et al
	Science 2010 327 (5964) 425-431
		
	“MuxViz: A Tool for Multilayer Analysis and Visualization of Networks”
	Manlio De Domenico, Mason A. Porter, and Alex Arenas
	Journal of Complex Networks 2015 3 (2) 159-176


that can be found at the following URLs:

<http://www.sciencemag.org/content/327/5964/425>

<http://comnet.oxfordjournals.org/content/3/2/159>

This work has been supported by European Commission FET-Proactive project PLEXMATH (Grant No. 317614), the European project devoted to the investigation of multi-level complex systems and has been developed at the Alephsys Lab. 

Visit

PLEXMATH: <http://www.plexmath.eu/>

ALEPHSYS: <http://deim.urv.cat/~alephsys/>

for further details.



### Description of the dataset

Multiplex in which the layers correspond to interaction networks of genes in Saccharomyces cerevisiae (which was obtained via a synthetic genetic-array methodology) and correlation-based networks in which genes with similar interaction profiles are connected to each other. Positive and negative interactions, as well as positive and negative correlations, are considered.

Thus, the provided layers are:

1. Positive interactions
2. Negative interactions
3. Positive correlations
4. Negative correlations

There are 4458 nodes, labelled with integer ID between 1 and 4458, and 8473997 connections.
The multiplex is undirected and unweighted, although in the file
    
    yeast_landscape_multiplex.edges

with format

    layerID nodeID nodeID r

are provided all the edges present in the original dataset. Here, r is either the genetic interaction score (for layers concerning interactions) or the Pearson correlation coefficient (for layers concerning correlations). 

###### NOTE: filter the interactions you might want to keep applying your preferred threshold to r

###### Don't use the edges list as it is.

The values for r are provided to allow flexible analyses. For further details see the original paper by Costanzo et al.


The IDs of all layers are stored in 

    yeast_landscape_layers.txt

The IDs of nodes, together with their name can be found in the file

    yeast_landscape_nodes.txt



### License

This YEAST LANDSCAPE MULTIPLEX DATASET is made available under the Open Database License: <http://opendatacommons.org/licenses/odbl/1.0/>. Any rights in individual contents of the database are licensed under the Database Contents License: <http://opendatacommons.org/licenses/dbcl/1.0/>

You should find a copy of the above licenses accompanying this dataset. If it is not the case, please contact us (see below).

A friendly summary of this license can be found here:

<http://opendatacommons.org/licenses/odbl/summary/>

and is reported in the following.

======================================================
ODC Open Database License (ODbL) Summary

This is a human-readable summary of the ODbL 1.0 license. Please see the disclaimer below.

You are free:

*    To Share: To copy, distribute and use the database.
*    To Create: To produce works from the database.
*    To Adapt: To modify, transform and build upon the database.

As long as you:
    
*	Attribute: You must attribute any public use of the database, or works produced from the database, in the manner specified in the ODbL. For any use or redistribution of the database, or works produced from it, you must make clear to others the license of the database and keep intact any notices on the original database.
    
*	Share-Alike: If you publicly use any adapted version of this database, or works produced from an adapted database, you must also offer that adapted database under the ODbL.
    
*	Keep open: If you redistribute the database, or an adapted version of it, then you may use technological measures that restrict the work (such as DRM) as long as you also redistribute a version without such measures.

======================================================


### Contacts

If you find any error in the dataset or you have questions, please contact

	Manlio De Domenico
	Universitat Rovira i Virgili 
	Tarragona (Spain)

email: <manlio.dedomenico@urv.cat>web: <http://deim.urv.cat/~manlio.dedomenico/>