## plot the entanglement results
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style("whitegrid")
sns.set_palette("Set2")
def parse_results_to_pandas(rfile,ent_type = "TYPE1", tag = "real"):

    rows = []
    nlx = set()
    with open(rfile) as rf:
        for line in rf:
            line = line.strip().replace("_NOVEL","").split("\t")
            if line[0] == "RESULT_LINE_"+ent_type:
                tagx, Intensity, Homogeneity, normalized, comp, layer_entanglement, dataset = line

#                nmx = str(n)+"_"+str(m)+"_"+str(p)+"_"+str(o)+"_"+str(q)+"_NOVEL.edges"
                dataset = dataset.split("/")[2]
                Discipline = dataset.split("_")[2]
                num_layers = dataset.split("_")[1]
                nlx.add(num_layers)
                oparam = dataset.split("_")[3]
                q = dataset.split("_")[-1].replace(".edges","")
                nodes = dataset.split("_")[0]
                row = {"Intensity":float(Intensity),"Homogeneity":float(Homogeneity),"normalized":float(normalized), "component":comp,"layer_entanglement":layer_entanglement,"dataset_whole":dataset,"Discipline":Discipline, "Num. layers":num_layers,"q":q,"nodes" : nodes,"o":oparam}
                rows.append(row)
                
            elif line[0] == "RESULT_LINE":
                tagx, Intensity, Homogeneity, normalized, comp, layer_entanglement, dataset = line

                Discipline = dataset.split("_")[2]
                row = {"Intensity":float(Intensity),"Homogeneity":float(Homogeneity),"normalized":float(normalized), "component":int(comp),"layer_entanglement":float(layer_entanglement),"dataset_whole":dataset,"Discipline":Discipline}
                rows.append(row)

    if ent_type == "TYPE2":
        print(rows[0])
        
    dfx1 = pd.DataFrame(rows)
    if tag == "real":
        dfx1 = dfx1.groupby(['dataset_whole','Discipline']).mean().reset_index()
        return dfx1
    
    gpb = [x for x in dfx1.columns if x != "component"]
    dfx1['normalized'] = dfx1['normalized'].astype(float)
    dfx1['Homogeneity'] = dfx1['Homogeneity'].astype(float)
    dfx1['Intensity'] = dfx1['Intensity'].astype(float)
    dfx1['q'] = dfx1['q'].astype(float)
#    dfx1['q2'] = dfx1['q2'].astype(float)
    dfx1['Num. layers'] = dfx1['Num. layers'].astype(int)
    dfx1['nodes'] = dfx1['nodes'].astype(float)
    dfx1['o'] = dfx1['o'].astype(float)
    dfx1['Discipline'] = dfx1['Discipline'].astype(float)
    dfx1['component'] = dfx1['component'].astype(float)
    dfx1 = dfx1.groupby(['Discipline','nodes','Num. layers','q','layer_entanglement','dataset_whole']).mean().reset_index()
    dfx1 = dfx1[dfx1['Intensity'] < 1]
    return dfx1

def basic_viz(dfx, tag = "real", misc_dataframe = None):

    if tag != "real":
        network_type = "Edge dropout"        
        dfx = dfx[dfx['Num. layers'] != "Multiplex"]

        dfx['n'] = dfx['nodes']
        
        dfx['Probability'] = dfx['Discipline'].astype(float).round(2)
        dfx = dfx[dfx['Homogeneity'] < 0.999]

        dfx['m'] = dfx['Num. layers'].astype(float)
        print(dfx.m.unique(),"unique",tag)
        sns.set_palette("Set1")
        #dfx['m'] = dfx['m'].astype(str).replace({v:float(k) for k,v in replacement_dict.items()})
        
        h = sns.jointplot(dfx['Probability'],dfx.Intensity, kind="hex")
        h.set_axis_labels('p', 'I', fontsize=15)
        print(dfx.columns)
        plt.savefig("figures/"+tag+"_Intensity.png",dpi=300)
        plt.clf()
        
        h = sns.jointplot(dfx['o'],dfx.Intensity, kind="hex")
        h.set_axis_labels('o', 'I', fontsize=15)
        #plt.xlabel("o",fontsize = 15)
        #plt.ylabel("I",fontsize = 15)
        print(dfx.columns)
        plt.savefig("figures/"+tag+"_Intensityo.png",dpi=300)
        plt.clf()

        h = sns.jointplot(dfx['o'],dfx.Homogeneity, kind="hex")
        h.set_axis_labels('o', 'H', fontsize=15)
        print(dfx.columns)
        plt.savefig("figures/"+tag+"_Homogeneityo.png",dpi=300)
        plt.clf()

        h = sns.jointplot(dfx['m'],dfx.Homogeneity, kind="hex")
        h.set_axis_labels('m', 'H', fontsize=15)
        print(dfx.columns)
        plt.savefig("figures/"+tag+"_Homogeneitym.png",dpi=300)
        plt.clf()

        h = sns.jointplot(dfx['m'],dfx.Intensity, kind="hex")
        h.set_axis_labels('m', 'I', fontsize=15)
        print(dfx.columns)
        plt.savefig("figures/"+tag+"_Intensitym.png",dpi=300)
        plt.clf()

        h = sns.jointplot(dfx['n'],dfx.Homogeneity, kind="hex")
        h.set_axis_labels('n', 'H', fontsize=15)
        print(dfx.columns)
        plt.savefig("figures/"+tag+"_Homogeneityn.png",dpi=300)
        plt.clf()

        h = sns.jointplot(dfx['n'],dfx.Intensity, kind="hex")
        h.set_axis_labels('n', 'I', fontsize=15)
        print(dfx.columns)
        plt.savefig("figures/"+tag+"_Intensityn.png",dpi=300)
        plt.clf()        
        
#        dfx = dfx.sort_values(by = ['Probability'])
        vx = np.array(dfx['Probability'].astype(float).values)
        
        slices = pd.cut(vx, bins = np.arange(0.1,1.1,0.1), retbins=True)[0]
        vlx = []
        for val in slices:
            vlx.append(str(val))
        dfx['Probability interval'] = vlx
        
        h = sns.jointplot(dfx['Probability'], dfx.Homogeneity, kind="hex")
        h.set_axis_labels('p', 'H', fontsize=15)
        plt.savefig("figures/"+tag+"_Homogeneity.png",dpi=300)
        plt.clf()

        dfx = dfx.fillna(0)
        dfx['q'] = dfx['q'].replace("nan","0.0")
        dfx['q'] = dfx['q'].replace(np.nan,"0.0")
        print(dfx.q.unique())
#        dfx = dfx.sort_values(by = ['q'])

        h = sns.jointplot(dfx['q'],dfx.Homogeneity, kind="hex")
        h.set_axis_labels('q', 'H', fontsize=15)
        plt.savefig("figures/"+tag+"_Homogeneityq.png",dpi=300)
        plt.clf()
        
        h = sns.jointplot(dfx['q'],dfx.Intensity,kind="hex")
        h.set_axis_labels('q', 'I', fontsize=15)
        plt.savefig("figures/"+tag+"_intensityq.png",dpi=300)
        plt.clf()
        
        slices1 = pd.cut(dfx.q.astype(float).values, bins = np.arange(0.1,1.1,0.1), retbins = True)[0]

        vlx1 = []
        for val in slices1:
            sx = str(val)
            if sx == np.nan or sx == "nan":
                sx = "nan"
            vlx1.append(sx)
        dfx['q'] = vlx1

        dfx = dfx[dfx['q'] != "nan"]
        
        sns.scatterplot("Homogeneity","Intensity", data= dfx, hue = dfx['q'].values,alpha = 0.3, s = 4)
        plt.legend(title = "q")
        plt.xlabel("H", fontsize = 15)
        plt.ylabel("I", fontsize = 15)
        plt.savefig("figures/"+tag+"_hom_int_q.png",dpi=300)
        plt.clf()


        replacement_dict = {"2":"[2]","4":"[4]","6":"[6]","8":"[8]","10":"[10]","3":"[3]","7":"[7]","9":"[9]","11":"[11]","5":"[5]"}
        dfx['m'] = dfx['Num. layers'].astype(str).replace(replacement_dict)
        
        sns.scatterplot("Homogeneity","Intensity", data= dfx, hue = 'm',alpha = 0.3, s = 4)
        
        plt.xlabel("H", fontsize = 15)
        plt.ylabel("I", fontsize = 15)
        plt.savefig("figures/"+tag+"_hom_int_layers.png",dpi=300)
        plt.clf()
        
        sns.set_palette("Set1")
        dfx = dfx.dropna()
        dfx = dfx[dfx['Probability interval'] != "nan"]
        print(dfx['Probability interval'].unique())
        dfx['p'] = dfx['Probability interval']
#        dfx = dfx.sort_values(by =["p"])
        
        sns.scatterplot("Homogeneity","Intensity", data= dfx, hue = dfx['p'],alpha = 0.3, s = 4)
        plt.xlabel("H",fontsize = 15)
        plt.ylabel("I",fontsize = 15)
        plt.savefig("figures/"+tag+"_hom_int_dropout.png",dpi=300)
        plt.clf()

#        sns.kdeplot(dfx["Homogeneity"],dfx["Intensity"], color = "lightgray", alpha = 0.3)
        sns.scatterplot("Homogeneity","Intensity", data= dfx, alpha = .5, s = 4, color = "lightgray")
        plt.xlabel("H",fontsize = 15)
        plt.ylabel("I",fontsize = 15)
        plt.savefig("figures/"+tag+"_GLOBALHI.png",dpi=300)
        plt.clf()
        
        print(dfx.nodes.unique())
        sns.set_palette("Set1")
        
#        dfx = dfx.sort_values(by = ['nodes'])
        dfx = dfx.sample(frac=1).reset_index(drop=True)
        slices1 = pd.cut(dfx.nodes.astype(int).values, bins = np.arange(10,max(dfx.nodes.values)+10,30), retbins = True)[0]

        vlx1 = []
        for val in slices1:
            sx = str(val)
            if sx == np.nan or sx == "nan":
                sx = "nan"
            vlx1.append(sx)
            
        dfx['n'] = vlx1
        dfx = dfx[dfx['n'] != "nan"]
        sns.scatterplot("Homogeneity","Intensity", data= dfx, hue = dfx['n'], alpha = .5, s = 4)
        plt.xlabel("H",fontsize = 15)
        plt.ylabel("I",fontsize = 15)
        # handles, labels = plt.gca().get_legend_handles_labels()
        # order = [5,6,3,2,1,4]
        # plt.legend([handles[idx] for idx in order],[labels[idx] for idx in order])
        plt.savefig("figures/"+tag+"_hom_int_nodes.png",dpi=300)
        plt.clf()
        
#        dfx = dfx.sort_values(by = ['o'])
        slices1 = pd.cut(dfx.o.astype(float).values, bins = np.arange(0.1,1.1,0.1), retbins = True)[0]
        vlx1 = []
        for val in slices1:
            sx = str(val)
            if sx == np.nan or sx == "nan":
                sx = "nan"
            vlx1.append(sx)
        dfx['o'] = vlx1
        dfx = dfx[dfx['o'] != "nan"]        
        sns.scatterplot("Homogeneity","Intensity", data= dfx, hue = dfx['o'],alpha = 0.3, s = 4)
        plt.xlabel("H",fontsize = 15)
        plt.ylabel("I",fontsize = 15)
        plt.savefig("figures/"+tag+"_hom_int_o.png",dpi=300)
        plt.clf()
        
    else:
        print("Plotting real")
        network_type = "Network type"
        dfx['Discipline'] = dfx['Discipline'].astype(str)
        sns.boxenplot(dfx.Discipline,dfx.Intensity)
        plt.xlabel(network_type, fontsize=15)
        plt.ylabel("I", fontsize=15)    
        plt.savefig("figures/"+tag+"_Intensity.png",dpi=300)
        plt.clf()
        sns.boxenplot(dfx.Discipline,dfx.Homogeneity)
        plt.xlabel(network_type, fontsize=15)
        plt.ylabel("H", fontsize=15)
        plt.savefig("figures/"+tag+"_Homogeneity.png",dpi=300)
        plt.clf()
        sns.boxenplot(dfx.Discipline,dfx.normalized)
        plt.xlabel(network_type, fontsize=15)
        plt.ylabel("Normalized Homogeneity", fontsize=15)
        plt.savefig("figures/"+tag+"_normalized.png",dpi=300)
        plt.clf()

        print(dfx.columns)
        plt.scatter(misc_dataframe.Homogeneity, misc_dataframe.Intensity, s = 3, alpha = 0.8, color = "black")
        plt.clf()

        
#        sns.kdeplot(dfx["Homogeneity"],dfx["Intensity"], color = "lightgray", alpha = 0.3)

        sns.scatterplot(x = "Homogeneity",y = "Intensity", hue = "Discipline", style = "Discipline",data = dfx, s = 80, alpha = 0.3)
        for k,v in dfx.iterrows():
            hom = v['Homogeneity']
            inte = v['Intensity']
            label = k#v['dataset_whole'].split("_")[0]
            print(v['dataset_whole'],k)
            plt.text(hom,inte,label,fontsize=9)
        plt.xlabel("H", fontsize=15)
        axes = plt.axes()
        axes.set_xlim([0, 1])
        plt.ylabel("I", fontsize=15)
        plt.savefig("figures/"+tag+"_hom_int.png",dpi=300)
        plt.clf()

        subset = dfx[dfx["dataset_whole"].str.contains("Social")]
        sns.distplot(subset['Intensity'], label = "Social")
        subset = dfx[dfx["dataset_whole"].str.contains("Genetic")]
        sns.distplot(subset['Intensity'], label = "Genetic")
        plt.ylabel("Density")
        plt.legend()
        plt.savefig("figures/"+tag+"_intensity_dist_.png",dpi=300)
        plt.clf()

        subset = dfx[dfx["dataset_whole"].str.contains("Social")]
        sns.distplot(subset['Homogeneity'], label = "Social")
        subset = dfx[dfx["dataset_whole"].str.contains("Genetic")]
        sns.distplot(subset['Homogeneity'], label = "Genetic")
        plt.ylabel("Density")
        plt.legend()
        plt.savefig("figures/"+tag+"_homogeneity_dist_.png",dpi=300)
        plt.clf()
        
    return dfx

def visualize_correlation(rfile):

    # ["RESULT_LINE_CORRELATION",mean_cor, max_cor, min_cor, std_cor, edgelist])
    rows = []
    with open(rfile) as rf:
        for line in rf:
            if "RESULT_LINE" in line:
                parts = line.strip().split("\t")
                #                nmx = str(n)+"_"+str(m)+"_"+str(p)+"_"+str(o)+"_"+str(q)+"_NOVEL.edges"
                tag, mean_cor, max_cor, min_cor, std_cor, dataset = parts
                dataset = dataset.split("/")[2]
                Discipline = dataset.split("_")[2]
                num_layers = dataset.split("_")[1]
                oparam = dataset.split("_")[3]
                q = dataset.split("_")[4]
                nodes = dataset.split("_")[0]
                row = {"dataset_whole":dataset,"p":Discipline, "Num. layers":num_layers,"q":q,"nodes" : nodes,"o":oparam,"mean_cor":mean_cor,"max_cor":max_cor,"min_cor":min_cor,"std_cor":std_cor}
                rows.append(row)
    dfx = pd.DataFrame(rows)
    dfx['min_cor'] = dfx['min_cor'].astype(float)
    dfx['max_cor'] = dfx['max_cor'].astype(float)
    dfx['mean_cor'] = dfx['mean_cor'].astype(float)
    dfx['o'] = dfx['o'].astype(float)
    dfx['p'] = dfx['p'].astype(float)
    dfx['nodes'] = dfx['nodes'].astype(float)
    dfx['q'] = dfx['q'].astype(float)
    dfx['std_cor'] = dfx['std_cor'].astype(float)
    dfx['Num. layers'] = dfx['Num. layers'].astype(float)
    for uln in dfx['Num. layers'].unique():
        subset = dfx[dfx['Num. layers'] == uln]
        sns.distplot(subset.mean_cor, rug = False, kde = False, label = str(int(uln)))
    plt.legend(title = "Num. layers")
    plt.xlabel("Mean correlation")
    plt.xlim(0,1)
    plt.tight_layout()
    plt.savefig("figures/correlation_mean.png", dpi = 300)
    plt.clf()
    for uln in dfx['Num. layers'].unique():
        subset = dfx[dfx['Num. layers'] == uln]
        sns.distplot(subset.max_cor, rug = False, kde = False, label = str(int(uln)))
    plt.legend(title = "Num. layers")
    plt.xlabel("Max correlation")
    plt.xlim(0,1)
    plt.tight_layout()
    plt.savefig("figures/correlation_max.png", dpi = 300)
    plt.clf()

    for uln in dfx['Num. layers'].unique():
        subset = dfx[dfx['Num. layers'] == uln]
        sns.distplot(subset.min_cor, rug = False, kde = False, label = str(int(uln)))
    plt.legend(title = "Num. layers")
    plt.xlabel("Min correlation")
    plt.xlim(0,1)
    plt.tight_layout()
    plt.savefig("figures/correlation_min.png", dpi = 300)
    plt.clf()

    for uln in dfx['Num. layers'].unique():
        subset = dfx[dfx['Num. layers'] == uln]
        sns.distplot(subset.std_cor, rug = False, kde = False, label = str(int(uln)))
    plt.legend(title = "Num. layers")
    plt.xlabel("Std correlation")
    plt.xlim(0,1)
    plt.tight_layout()
    plt.savefig("figures/correlation_std.png", dpi = 300)
    plt.clf()

    sns.lineplot(dfx['Num. layers'], dfx.mean_cor)
    plt.xlabel("Number of layers")
    plt.ylabel("Pairwise correlation (mean)")
    plt.savefig("figures/correlation_dependence.png", dpi = 300)
    plt.clf()

    sns.lineplot(dfx['q'], dfx.mean_cor)
    plt.xlabel("q parameter")
    plt.ylabel("Pairwise correlation (mean)")
    plt.tight_layout()
    plt.savefig("figures/correlation_q.png", dpi = 300)
    plt.clf()

    sns.lineplot(dfx['o'], dfx.mean_cor)
    plt.xlabel("o parameter")
    plt.ylabel("Pairwise correlation (mean)")
    plt.tight_layout()
    plt.savefig("figures/correlation_o.png", dpi = 300)
    plt.clf()

    sns.lineplot(dfx['p'], dfx.mean_cor)
    plt.xlabel("p parameter")
    plt.ylabel("Pairwise correlation (mean)")
    plt.tight_layout()
    plt.savefig("figures/correlation_p.png", dpi = 300)
    plt.clf()

    sns.lineplot(dfx['nodes'], dfx.mean_cor)
    plt.xlabel("Number of nodes")
    plt.ylabel("Pairwise correlation (mean)")
    plt.tight_layout()
    plt.savefig("figures/correlation_n.png", dpi = 300)
    plt.clf()
    
if __name__ == "__main__":

    import argparse    
    parser = argparse.ArgumentParser()
    parser.add_argument("--results",default="./results_clean/correlation14.txt", type = str)
    parser.add_argument("--measure",default="correlation", type = str)
    args = parser.parse_args()
    
    result_file = args.results

    if args.measure == "entanglement":
        tag = "synthetic_TYPE1"
        dfx = parse_results_to_pandas(result_file, tag = tag)
        print(dfx.shape)
        datafr = basic_viz(dfx,tag=tag)

        tag = "real"
        dfx = parse_results_to_pandas("results_clean/second_run.txt", tag = tag)
        print(dfx.shape)
        datafr = basic_viz(dfx,tag=tag, misc_dataframe = datafr)

        tag = "synthetic_TYPE2"
        dfx = parse_results_to_pandas(result_file,ent_type = "TYPE2", tag = tag)
        print(dfx.shape)
        datafr = basic_viz(dfx,tag=tag)

        tag = "synthetic_TYPE3"
        dfx = parse_results_to_pandas(result_file,ent_type = "TYPE3", tag = tag)
        print(dfx.shape)
        datafr = basic_viz(dfx,tag=tag)
    elif args.measure == "correlation":
        
        visualize_correlation(result_file)
