## minimum example
from py3plex.core import multinet
from entanglement import *
#from py3plex.algorithms.multilayer_algorithms.entanglement import *
import tqdm

def return_entanglement_results(edgelist, synthetic = False):

    ## visualization from a simple filey
    try:
        multilayer_network = multinet.multi_layer_network(network_type = "multilayer").load_network(edgelist, directed=True, input_type="multiplex_edges")
        multilayer_network.basic_stats()
        edges_coupled = list(multilayer_network.get_edges(multiplex_edges = False))
        edges_coupled = len(list(multilayer_network.get_edges(multiplex_edges = False)))        
        dataset = edgelist
        analysis = compute_entanglement_analysis(multilayer_network, entanglement_type = "elementary")
        
        #    print ("%d connected components of layers"%len(analysis))
        
        for i,b in enumerate(analysis):
            out_row = "RESULT_LINE_TYPE1\t"+"\t".join(str(x) for x in [b['Entanglement intensity'],b['Entanglement homogeneity'],b['Normalized homogeneity'],i,len(b['Layer entanglement']),dataset])
            print(out_row)

        analysis = compute_entanglement_analysis(multilayer_network, entanglement_type = "transition")
        #    print ("%d connected components of layers"%len(analysis))
        
        for i,b in enumerate(analysis):
            out_row = "RESULT_LINE_TYPE2\t"+"\t".join(str(x) for x in [b['Entanglement intensity'],b['Entanglement homogeneity'],b['Normalized homogeneity'],i,len(b['Layer entanglement']),dataset])
            print(out_row)

        analysis = compute_entanglement_analysis(multilayer_network, entanglement_type = "both")
        #    print ("%d connected components of layers"%len(analysis))
        
        for i,b in enumerate(analysis):
            out_row = "RESULT_LINE_TYPE3\t"+"\t".join(str(x) for x in [b['Entanglement intensity'],b['Entanglement homogeneity'],b['Normalized homogeneity'],i,len(b['Layer entanglement']),dataset])
            print(out_row)
            
    except Exception as ex:
        print(ex)

if __name__ == "__main__":

    import glob
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset",default="../synthetic_multiplex/100_3_0.11_False_0.5_0.5.edges", type = str)
    args = parser.parse_args()
    
    ## real    
    # for filename in glob.glob("../data/*/Dataset/*.edges"):
    #     return_entanglement_results(filename)
    
    ## synthetic
    return_entanglement_results(args.dataset, synthetic = True)
