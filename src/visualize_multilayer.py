## multilayer
from py3plex.visualization.multilayer import *
from py3plex.visualization.colors import all_color_names,colors_default
from py3plex.core import multinet
from entanglement import *
from generate import *
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style("whitegrid")
sns.set_palette("Set2")

def generate(n,m,o,p,q1,bool_loops = False):
    edges_only = random_multiplex_generator(int(n), int(m), o = o , p = p, q = q1, allow_loops = bool_loops)['edges']
    edgelist = []
    for k,v in edges_only.items():

        for l in v:

            edgelist.append(" ".join([str(l),str(k[0]),str(k[1]),str(1)]))

    edgelist = "\n".join(edgelist)
    fx = open("../synthetic_multiplex/{}".format("tmp.edgelist"),"w+")
    fx.write(edgelist)
    fx.close()

def deparse(filename):

    fx = open(filename, "r")
    edgelist = fx.read().split("\n")
    final_edgelist = []
    for lx in edgelist:

        lx = lx.split(" ")
        if "_" in lx[0]:
            l1,l2 = lx[0].split("_")
        else:
            continue
            l1 = lx[0]
            l2 = lx[0]
        n1 = lx[2]
        n2 = lx[3]
        out_edge = " ".join((n1,l1,n2,l2))
        final_edgelist.append(out_edge)
    fx2 = open("../synthetic_multiplex/tmp2.edgelist","w")
    fx2.write("\n".join(final_edgelist))
    fx2.close()

def parse_results_to_pandas(rfile,ent_type = "TYPE1", tag = "synthetic"):
    
    flx = pd.read_csv("./results_clean/extracts.tsv",sep = "\t")
    flx.columns = ['tag','Intensity','Homogeneity','nx','it','ent','dataset']
    dstring = flx.dataset
    key_data = []
    for index, row in flx.iterrows():
        intensity = row['Intensity']
        homogeneity = row['Homogeneity']
        n,m,o,p,q = row['dataset'].split("/")[-1].replace(".edges","").replace("_NOVEL","").split("_")
        if float(m) > 3 and float(q) > 0.7 and float(n) > 20 and homogeneity < 1 and intensity > 0.8:
            key_data.append([float(intensity),float(homogeneity),int(n),int(m),float(o),float(p),float(q)])
    dfx2 = pd.DataFrame(key_data)
    dfx2.columns = ['I','H','n','m','o','p','q']
    return dfx2

## you can try the default visualization options --- this is the simplest option/


sorting_factor = "I"
q_flag = "transition"
pandas_dfx = parse_results_to_pandas("results_clean/multilayer12.txt")
psx = pandas_dfx.sort_values([sorting_factor], ascending = False)
r0 = psx.iloc[2]
tpx = (int(r0.n),int(r0.m),r0.o,r0.p,r0.q)
I = r0.I
H = r0.H
print(tpx,I,H)
generate(tpx[0],tpx[1],tpx[2],tpx[3],tpx[4])
edgelist = "../synthetic_multiplex/tmp.edgelist"
deparse(edgelist)
edgelist = "../synthetic_multiplex/tmp2.edgelist"
print("generated! {}".format(tpx))
index_tag = 0
try:
    ## reparese edgelist so it actually generates multilayer links.
    multilayer_network = multinet.multi_layer_network(network_type = "multilayer").load_network(edgelist, directed=False, input_type="multiedgelist")
    multilayer_network.visualize_network(style="diagonal")
    analysis = compute_entanglement_analysis(multilayer_network, entanglement_type = q_flag)
    arx = []
    for i,b in enumerate(analysis):
        x = (b['Entanglement intensity'],b['Entanglement homogeneity'])
        arx.append(x)

    outstr = "$I$: {}\n$H$: {}".format(str(round(I,2)),str(round(H,2)))
    plt.text(int(3 * r0.m/4),0.1,outstr,fontsize = 15)
    ofn = "../visualizations/{}_{}_{}.png".format(str(q_flag),sorting_factor, "_".join([str(x) for x in tpx]))
    plt.savefig(ofn,dpi=300)
except Exception as es:
    print(es)
    index_tag += 1
    pass
