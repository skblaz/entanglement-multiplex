## temporal part of the study

#mkdir ../images;
#rm -rvf ../images/*;

#for j in ../temporal_data/*;do echo "python3 compute_temporal_entanglement.py --dataset $j";done
#A=$(for j in ../temporal_data/*;do echo "python3 compute_temporal_entanglement.py --dataset $j,";done);echo $A | mawk -v RS='[,\n]' '{print $0}' | parallel

## ID
ID=14

## non-temporal part of the study
rm -rf ../synthetic_multiplex;
mkdir ../synthetic_multiplex;
python3 generate.py;

## correlation analysis
ls ../synthetic_multiplex/ | mawk '{print "python3 compute_correlation.py ""--dataset ../synthetic_multiplex/"$1}' | parallel --progress >> ./results_clean/correlation$ID.txt

## entanglement analysis
ls ../synthetic_multiplex/ | mawk '{print "python3 compute_entanglement.py ""--dataset ../synthetic_multiplex/"$1}' | parallel --progress >> ./results_clean/multilayer$ID.txt
python3 plot_results.py --results ./results_clean/multilayer$ID.txt
