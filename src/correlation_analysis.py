import sys
print(sys.version)

import networkx as nx
from scipy.sparse.csgraph import csgraph_from_dense, connected_components
from collections import defaultdict
from scipy import spatial
from enum import Enum
import numpy as np
import itertools
import math

# Build the R and C matrix
def build_occurrence_matrix(network, entanglement_type = "elementary"):

    multiedges = network.get_edges()
    layers = []
    edge_list = []
    
    for e in multiedges:

        if entanglement_type == "both":
            pass
        else:
             if entanglement_type == "elementary":
                 if "_" in e[0][1] or "_" in e[1][1]:
                     continue

             if entanglement_type == "transition":
                 if not "_" in e[0][1] or not "_" in e[1][1]:
                     continue
                            
        if len(e) < 3:
            (n1,l1), (n2,l2) = e
            
        else:
            (n1,l1), (n2,l2), w = e
        
        if l1 == l2:
            if l1 not in layers:
                layers += [l1]
            edge_list.append([n1,n2,l1])            

    edge_list = sorted(edge_list, key=lambda x: [x[0],x[1]])
    nb_layers = len(layers)
    return edge_list, nb_layers

def compute_correlation_distribution(multiplex_network):

    edges, nl = build_occurrence_matrix(multiplex_network, entanglement_type = "elementary")
    possible_layers = set()
    layermaps = defaultdict(set)
    for link in edges:
        possible_layers.add(link[2])
        layermaps[link[2]].add(link[1])
        layermaps[link[2]].add(link[0])        
    qdist = []
    for pair in itertools.combinations(possible_layers, 2):
        ll1 = layermaps[pair[0]]
        ll2 = layermaps[pair[1]]
        Q = len(ll1.intersection(ll2))/len(ll1.union(ll2))
        qdist.append(Q)
    return qdist
