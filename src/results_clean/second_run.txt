Imported BH algo
algorithms imported..
Core statistics imported..
3.6.8 |Anaconda, Inc.| (default, Dec 30 2018, 01:22:34) 
[GCC 7.3.0]
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 87
Number of edges: 740
Average in degree:   8.5057
Average out degree:   8.5057
RESULT_LINE	0.7053722041684737	0.9689083235878881	0.840834511962234	0	3	Vickers-Chan-7thGraders_Multiplex_Social
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 10178
Number of edges: 63677
Average in degree:   6.2563
Average out degree:   6.2563
RESULT_LINE	0.07975569213403995	0.407135233699478	0.26694445835119396	0	7	SacchPomb_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 27994
Number of edges: 282755
Average in degree:  10.1006
Average out degree:  10.1006
RESULT_LINE	0.07042791522549165	0.695150293776376	0.48932438767475117	0	7	SacchCere_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 369
Number of edges: 322
Average in degree:   0.8726
Average out degree:   0.8726
RESULT_LINE	0.16034131784377836	0.5820149844962725	0.39547101186485456	0	4	Bos_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 2034
Number of edges: 3588
Average in degree:   1.7640
Average out degree:   1.7640
RESULT_LINE	0.015498672487816185	0.743443410895036	0.5336174819142498	0	37	EUAir_Multiplex_Transport
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 1195
Number of edges: 1355
Average in degree:   1.1339
Average out degree:   1.1339
RESULT_LINE	0.15834664098159507	0.5836478791034291	0.3967502910722296	0	5	HumanHIV1_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 26796
Number of edges: 59026
Average in degree:   2.2028
Average out degree:   2.2028
RESULT_LINE	0.11478631594524064	0.6416704378490089	0.44351658796076754	0	13	arXiv-Netscience_Multiplex_Coauthorship
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 151
Number of edges: 144
Average in degree:   0.9536
Average out degree:   0.9536
RESULT_LINE	0.2413220165241806	0.6359429052925407	0.4387772569219466	0	3	Oryctolagus_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 659951
Number of edges: 991854
Average in degree:   1.5029
Average out degree:   1.5029
RESULT_LINE	0.2691590900398969	0.900587388848829	0.7137264957046515	0	3	Cannes2013_Multiplex_Social
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 1206
Number of edges: 2522
Average in degree:   2.0912
Average out degree:   2.0912
RESULT_LINE	0.4988503844246519	0.7073889774067658	0.5002541165854419	0	2	Plasmodium_Multiplex_Genetic
RESULT_LINE	0.0003965107057890563	1.0	1.0	1	1	Plasmodium_Multiplex_Genetic
list index out of range
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 399
Number of edges: 441
Average in degree:   1.1053
Average out degree:   1.1053
RESULT_LINE	0.3662755727963792	0.7516750716994502	0.5415076315771974	0	2	London_Multiplex_Transport
RESULT_LINE	0.10672853828306264	1.0	1.0	1	1	London_Multiplex_Transport
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 63
Number of edges: 312
Average in degree:   4.9524
Average out degree:   4.9524
RESULT_LINE	0.41287469524278225	0.8387908249361689	0.6334739236935814	0	3	Krackhardt-High-Tech_Multiplex_Social
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 392542
Number of edges: 396671
Average in degree:   1.0105
Average out degree:   1.0105
RESULT_LINE	0.2600993196134664	0.6244263086740187	0.42933436015142457	0	3	MLKing2013_Multiplex_Social
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 41713
Number of edges: 318346
Average in degree:   7.6318
Average out degree:   7.6318
RESULT_LINE	0.2900179420303115	0.8438473681444549	0.6394294490485138	0	364	FAO_Multiplex_Trade
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 367
Number of edges: 389
Average in degree:   1.0599
Average out degree:   1.0599
RESULT_LINE	0.1518451421076809	0.4333738641293934	0.2853540598414924	0	6	Gallus_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 133619
Number of edges: 210250
Average in degree:   1.5735
Average out degree:   1.5735
RESULT_LINE	0.24632076331941222	0.8805198246762096	0.6856125918493211	0	3	MoscowAthletics2013_Multiplex_Social
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 211
Number of edges: 2571
Average in degree:  12.1848
Average out degree:  12.1848
RESULT_LINE	0.5162319923075384	0.9703638919670763	0.8446239245853804	0	3	Lazega-Law-Firm_Multiplex_Social
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 674
Number of edges: 1551
Average in degree:   2.3012
Average out degree:   2.3012
RESULT_LINE	0.394666051381664	0.988308540714867	0.9025563553174835	0	3	CKM-Physicians-Innovation_Multiplex_Social
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 4557
Number of edges: 8182
Average in degree:   1.7955
Average out degree:   1.7955
RESULT_LINE	0.11571840898638563	0.4202310055292464	0.27610191878836043	0	6	Celegans_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 3457453
Number of edges: 4061960
Average in degree:   1.1748
Average out degree:   1.1748
RESULT_LINE	0.3162020453033128	0.8354691746218633	0.6296080390018515	0	3	ObamaInIsrael2013_Multiplex_Social
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 965
Number of edges: 7153
Average in degree:   7.4124
Average out degree:   7.4124
RESULT_LINE	0.08655081790710384	0.7161561365375919	0.5082003226372143	0	16	PierreAuger_Multiplex_Coauthorship
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 418
Number of edges: 398
Average in degree:   0.9522
Average out degree:   0.9522
RESULT_LINE	0.3449037848596161	0.7110533510721576	0.5035631512902534	0	2	Candida_Multiplex_Genetic
RESULT_LINE	0.1539187913341401	0.9689238237556534	0.8408743992941738	1	3	Candida_Multiplex_Genetic
RESULT_LINE	0.3555276869109409	0.9854498918022643	0.8912682873973524	2	2	Candida_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 180
Number of edges: 188
Average in degree:   1.0444
Average out degree:   1.0444
RESULT_LINE	0.0111731843575419	1.0	1.0	0	1	DanioRerio_Multiplex_Genetic
RESULT_LINE	0.12526563272584224	0.7406081156056138	0.5309249275674581	1	4	DanioRerio_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 26
Number of edges: 35
Average in degree:   1.3462
Average out degree:   1.3462
RESULT_LINE	0.5477154714877635	0.9864332510323383	0.895015486949587	0	2	Padgett-Florence-Families_Multiplex_Social
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 261
Number of edges: 259
Average in degree:   0.9923
Average out degree:   0.9923
RESULT_LINE	0.24597906164162686	0.5950370551704371	0.40572526359120953	0	4	HumanHerpes4_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 36194
Number of edges: 170899
Average in degree:   4.7217
Average out degree:   4.7217
RESULT_LINE	0.10104723671994452	0.519648058241546	0.3478738544610215	0	7	Homo_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 11970
Number of edges: 43367
Average in degree:   3.6230
Average out degree:   3.6230
RESULT_LINE	0.08228336327267406	0.405509440148796	0.2658117246581573	0	7	Drosophila_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 791
Number of edges: 5863
Average in degree:   7.4121
Average out degree:   7.4121
RESULT_LINE	0.3394609624571128	0.8563729163577208	0.6545749026969971	0	3	CElegans_Multiplex_Neuronal
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 129
Number of edges: 137
Average in degree:   1.0620
Average out degree:   1.0620
RESULT_LINE	0.30467941828602135	0.7773820943476367	0.5669055937956823	0	3	HepatitusCVirus_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 17770
Number of edges: 8473997
Average in degree: 476.8710
Average out degree: 476.8710
RESULT_LINE	0.13203469857208774	0.534029560156226	0.35864580537718604	0	4	YeastLandscape_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 150
Number of edges: 1018
Average in degree:   6.7867
Average out degree:   6.7867
RESULT_LINE	0.438508882701763	0.9101681399126264	0.7280955285120894	0	4	Kapferer-Tailor-Shop_Multiplex_Social
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 224
Number of edges: 620
Average in degree:   2.7679
Average out degree:   2.7679
RESULT_LINE	0.34138811384809453	0.8947659663162345	0.7053153857816885	0	5	CS-Aarhus_Multiplex_Social
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 582
Number of edges: 620
Average in degree:   1.0653
Average out degree:   1.0653
RESULT_LINE	0.15311163590625754	0.6589319701019981	0.45798299492359373	0	4	Xenopus_Multiplex_Genetic
RESULT_LINE	0.011965811965811967	1.0	1.0	1	1	Xenopus_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 8765
Number of edges: 18655
Average in degree:   2.1284
Average out degree:   2.1284
RESULT_LINE	0.11163632042206047	0.4089395696413278	0.26820264017465656	0	7	Arabidopsis_Multiplex_Genetic
-------------------- 
 Computing core stats of the network 
 --------------------
Name: 
Type: MultiDiGraph
Number of nodes: 3263
Number of edges: 4268
Average in degree:   1.3080
Average out degree:   1.3080
RESULT_LINE	0.12688865414412934	0.4578879688816304	0.3027878416342237	0	6	Rattus_Multiplex_Genetic
