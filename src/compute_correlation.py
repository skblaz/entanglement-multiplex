## minimum example
from py3plex.core import multinet
import tqdm
from correlation_analysis import *

def return_correlation_results(edgelist, synthetic = False):

    ## visualization from a simple filey
    try:
        multilayer_network = multinet.multi_layer_network(network_type = "multilayer").load_network(edgelist, directed=True, input_type="multiplex_edges")
        multilayer_network.basic_stats()

        correlation_distribution = compute_correlation_distribution(multilayer_network)
        mean_cor = np.mean(correlation_distribution)
        max_cor = np.max(correlation_distribution)
        min_cor = np.min(correlation_distribution)
        std_cor = np.std(correlation_distribution)
        out_row = "\t".join(str(x) for x in ["RESULT_LINE_CORRELATION",mean_cor, max_cor, min_cor, std_cor, edgelist])
        print(out_row)
            
    except Exception as ex:
        print(ex)

if __name__ == "__main__":

    import glob
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset",default="../synthetic_multiplex/10_2_0.4_0.9_0.7000000000000001_NOVEL.edges", type = str)
    args = parser.parse_args()
    
    ## synthetic
    return_correlation_results(args.dataset, synthetic = True)
