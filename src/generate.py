import random
import itertools
import tqdm
import numpy as np
#generate a multiplex network from a random bipartite graph

#n: number of nodes (int)
#m: number of layers (int)
#p: intra-layer edge probability (float [0..1])
#q: inter-layer same-node edge probability (float [0..1])
#r: inter-layer different-nodes edge probability (float [0..1])
#allow_loops: allows loops in intra-layer edges
#nodes_on_every_layer: assigns all n nodes to each m layers


def random_multiplex_generator(v, k, o = 1, p = 0.9, q = 0, r = 0, allow_loops = False):

    layers = range(k)
    node_to_layers = {}
    layer_to_nodes = {}

    # generate nodes and assign them to layers
    for node in range(v):
        layer_list = random.sample(layers, int(o*k))#random.choice(layers))
        node_to_layers[node] = layer_list
        for l in layer_list:
            layer_to_nodes[l] = layer_to_nodes.get(l, []) + [node]


    # for each layer
    edge_to_layers = {}
    for l, nlist in layer_to_nodes.items():

        # consider all nodes connected in a clique
        if allow_loops:
            clique = tuple(itertools.combinations_with_replacement(nlist, 2))
        else:
            clique = tuple(itertools.combinations(nlist, 2))
        nnodes = len(nlist)

        # select only a sample of them based on the edge probability parameter
        edge_sample = random.sample(clique, int((p) * (nnodes * (nnodes-1)) / 2))
        for p1, p2 in edge_sample:
            if p1 < p2:
                e = tuple([p1, p2])
            else:
                e = tuple([p2, p1])

            edge_to_layers[e] = edge_to_layers.get(e, []) + [l]


    if q == 0 and r == 0:
        return {'nodes': node_to_layers, 'edges': edge_to_layers}


    # the inter-layer connections
    inter_layers = []
    # for each pair of layers
    for l1, l2 in itertools.combinations(layers, 2):

        # find the nodes shared between them
        if l1 in layer_to_nodes and l2 in layer_to_nodes:
            pass
        else:
            continue
        nlist = set(layer_to_nodes[l1]).union(set(layer_to_nodes[l2]))
        nnodes = len(nlist)

        # create dummy layers representing the interlayer space
        inter_layer = "%d_%d"%(l1, l2)
        inter_layers += [inter_layer]

        # sample the nodes that are interconnected based on the same node interlayer edge probability
        for node in random.sample(nlist, int((q) * nnodes)):
            e = tuple([node, node])
            edge_to_layers[e] = edge_to_layers.get(e, []) + [inter_layer]

        # sample the nodes that are interconnected based on the different node interlayer edge probability
        clique = tuple(itertools.combinations(nlist, 2))
        edge_sample = random.sample(clique, int((r) * (nnodes * (nnodes - 1)) / 2))
        for p1, p2 in edge_sample:
            if p1 < p2:
                e = tuple([p1, p2])
            else:
                e = tuple([p2, p1])

            edge_to_layers[e] = edge_to_layers.get(e, []) + [inter_layer]


    return {'nodes': node_to_layers, 'edges': edge_to_layers}


#print random_multiplex_generator(10, 10, 0.9, 0.9, 0, allow_loops = True)


# n = 5
# m = 3
# p = 1
# q = 0
# edges_only = random_multiplex_generator(int(n), int(m), p = p, q = q, allow_loops = False)['edges']

# edgelist = []

# for k,v in edges_only.items():

#    for l in v:

#        edgelist.append(" ".join([str(l),str(k[0]),str(k[1]),str(1)]))

# edgelist = "\n".join(edgelist)
# nmx = str(n)+"_"+str(m)+"_"+str(p)+"_"+str(False)+"_"+str(q)+".edges"
# fx = open("./{}".format(nmx),"w+")
# fx.write(edgelist)
# fx.close()

pbar = tqdm.tqdm()
for n in np.arange(10,200,10):
    for m in np.arange(0,11,1):
        for p in np.arange(0.1,1,0.1):
            for q1 in np.arange(0.1,1,0.1):
                for bool_loops in [False]:
                    for o in np.arange(0.1,1,0.1):
                        pbar.update(1)
                        # v, k, o = 1, p = 0.9, q = 0, r = 0, allow_loops = False
                        edges_only = random_multiplex_generator(int(n), int(m), o = o , p = p, q = q1, allow_loops = bool_loops)['edges']
                        edgelist = []
                        for k,v in edges_only.items():
                            for l in v:
                                edgelist.append(" ".join([str(l),str(k[0]),str(k[1]),str(1)]))
                        edgelist = "\n".join(edgelist)
                        nmx = str(n)+"_"+str(m)+"_"+str(p)+"_"+str(o)+"_"+str(q1)+"_NOVEL.edges"
                        fx = open("../synthetic_multiplex/{}".format(nmx),"w+")
                        fx.write(edgelist)
                        fx.close()
                        pbar.update(1)
