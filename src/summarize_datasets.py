## compute entanglement
## entanglement: By. Benjamin Renoust and Blaz Skrlj, 2019

from py3plex.core import multinet
from py3plex.algorithms.multilayer_algorithms.entanglement import *
import numpy as np

def summarize_network(edgelist):

    ## visualization from a simple filey
    try:
        multilayer_network = multinet.multi_layer_network("multiplex").load_network(edgelist, directed=True, input_type="multiplex_edges")
        row = multilayer_network.summary()
        dataset = edgelist.split("/")[2]
        row['dataset'] = dataset.split("_")[0]
        row['type'] = dataset.split("_")[-1]
        return row
    except Exception as es:
        print(es)


if __name__ == "__main__":

    import glob
    import pandas as pd

    datapoints = []
    for filename in glob.glob("../data/*/Dataset/*.edges"):
        print(filename)
        try:            
            datapoint = summarize_network(filename)
            print(datapoint)
            if datapoint:
                datapoint['Mean degree'] = np.round(datapoint['Mean degree'],2)
                datapoints.append(datapoint)
        except:
            pass
    dfx = pd.DataFrame(datapoints)[['dataset','type','Nodes','Edges','Number of layers','Mean degree','CC']]
    dfx.to_latex("tables/summary.tex",index=False)
