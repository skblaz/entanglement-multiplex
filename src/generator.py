import random
import itertools
import tqdm
import numpy as np

#generate a multiplex network from a random bipartite graph
#n: number of nodes (int)
#m: number of layers (int)
#d: layer dropout (to avoid cliques) (float [0..1])

def random_multiplex_generator(n, m, d=0.9):
    layers = range(m)
    node_to_layers = {}
    layer_to_nodes = {}

    for node in range(n):
        layer_list = random.sample(layers, random.choice(layers))
        node_to_layers[node] = layer_list
        for l in layer_list:
            layer_to_nodes[l] = layer_to_nodes.get(l, []) + [node]

    edge_to_layers = {}
    for l, nlist in layer_to_nodes.items():
        clique = tuple(itertools.combinations(nlist,2))
        nnodes = len(nlist)
        edge_sample = random.sample(clique, int(((1-d)*(nnodes*(nnodes-1))/2))
        for p1,p2 in edge_sample:
            if p1<p2:
                e = tuple([p1,p2])
            else:
                e = tuple([p2,p1])

            edge_to_layers[e] = edge_to_layers.get(e, []) + [l]

    return {'nodes':node_to_layers, 'edges':edge_to_layers}

for n in tqdm.tqdm([10,25,50,100,250,500,1000,2500]):
    for m in [3,4,5,6,7,8,9,10]:
        for d in np.arange(0.001,0.9,0.01):
            edges_only = random_multiplex_generator(n, m, d)['edges']
            edgelist = []
            for k,v in edges_only.items():
                for l in v:
                    edgelist.append(" ".join([str(l),str(k[0]),str(k[1]),str(1)]))
            edgelist = "\n".join(edgelist)
            nmx = str(n)+"_"+str(m)+"_"+str(d)+".edges"
            fx = open("./synthetic/{}".format(nmx),"w+")
            fx.write(edgelist)
            fx.close()            
