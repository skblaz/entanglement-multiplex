### analyse a temporal network
## compute entanglement
## entanglement: By. Benjamin Renoust and Blaz Skrlj, 2019

from py3plex.core import multinet
import pandas as pd
pd.options.mode.chained_assignment = None 
from entanglement import *
import tqdm
from py3plex.visualization.multilayer import *
from py3plex.visualization.colors import all_color_names,colors_default
from py3plex.core import multinet
from collections import defaultdict
import time
import matplotlib.pyplot as plt
import seaborn as sns
import copy

def return_entanglement_results(multiplex_folder, resolution = 1000, mode = "slice", downsample = 1, window_size = 5):

    
    ## first parse the layer n1 n2 w edgelist
    multilayer_network = multinet.multi_layer_network().load_network(multiplex_folder+"/multiplex.edges",directed=True, input_type="multiplex_edges")

    ## map layer ids to names
    multilayer_network.load_layer_name_mapping(multiplex_folder+"/layers.txt", header = True)

    multilayer_network.split_to_layers(style="none", multiplex=True)
    
    ## remove all internal networks' edges.
    multilayer_network.remove_layer_edges() ## empty graphs are stored as self.empty_layers

    ## Finally, load termporal edge information
    multilayer_network.load_network_activity(multiplex_folder+"/activity.txt")
    dfx_time = multilayer_network.activity

    ## subsample
    # multilayer_network.activity = multilayer_network.activity.iloc[::downsample, :]

    ## normalize time
    multilayer_network.activity['timestamp'] = multilayer_network.activity['timestamp'].astype(int) - min(multilayer_network.activity['timestamp'].astype(int))
    
    times = multilayer_network.activity['timestamp'].astype(int)
    multilayer_network.activity['weight'] = np.repeat(1,len(times))

    ## split time series into resolution different intervals
    intervals = np.arange(0, max(times), int(max(times)/resolution))
    partial_slices_overall = []

    ## for each interval, extract all edges
    for j in tqdm.tqdm(range(len(intervals)-1)):
        q0 = intervals[j]
        q1 = intervals[j+1]
        subframe = multilayer_network.activity[(multilayer_network.activity['timestamp'] >= q0) & (multilayer_network.activity['timestamp'] < q1)]
        partial_slices_overall.append(subframe)

    n = resolution #chunk row size
    partial_slices = partial_slices_overall
    num_edges = defaultdict(list)
    temporal_maps = []
    for enx, time_slice in tqdm.tqdm(enumerate(partial_slices), total = len(partial_slices)):
        tmp_multiplex_edges = multilayer_network.edges_from_temporal_table(time_slice)
        multilayer_network = multinet.multi_layer_network().load_network(tmp_multiplex_edges, directed=False, input_type="multiedge_tuple_list")
        analysis = compute_entanglement_analysis(multilayer_network)
        for i,b in enumerate(analysis):
            intensity = b['Entanglement intensity']
            normalized_homogeneity = b['Entanglement homogeneity']
            layer_entanglement = b['Layer entanglement']
            temporal_maps.append({"time": enx, "Intensity":intensity, "NormHom": normalized_homogeneity})

    print("Starting time window decomposition!")
    temporal_maps_window = []    
    for enx, time_slice in tqdm.tqdm(enumerate(partial_slices), total = len(partial_slices)):
        if enx - window_size >= window_size:
            time_slice_table = pd.concat(partial_slices[(enx-window_size):enx], axis = 0).drop_duplicates()
            tmp_multiplex_edges = multilayer_network.edges_from_temporal_table(time_slice_table)
            multilayer_network = multinet.multi_layer_network().load_network(tmp_multiplex_edges, directed=True, input_type="multiedge_tuple_list")
            analysis = compute_entanglement_analysis(multilayer_network)
            for i,b in enumerate(analysis):
                intensity = b['Entanglement intensity']
                normalized_homogeneity = b['Entanglement homogeneity']
                layer_entanglement = b['Layer entanglement']
                temporal_maps_window.append({"time": enx, "Intensity":intensity, "NormHom": normalized_homogeneity})
        else:
            temporal_maps_window.append({"time": enx, "Intensity":0, "NormHom": 0})
            
    print("Summarizing")
    fframe = pd.DataFrame(temporal_maps)
    fframe['Intensity'] = fframe['Intensity']/fframe['Intensity'].max()
    fframe['NormHom'] = fframe['NormHom']/fframe['NormHom'].max()
    fframe_window = pd.DataFrame(temporal_maps_window)
    fframe_window['Intensity'] = fframe_window['Intensity']/fframe_window['Intensity'].max()
    fframe_window['NormHom'] = fframe_window['NormHom']/fframe_window['NormHom'].max()
    return fframe, fframe_window, partial_slices_overall

if __name__ == "__main__":

    import glob
    import matplotlib.pyplot as plt
    import seaborn as sns
    import pandas as pd
    from collections import Counter
    from dateutil.easter import easter
    sns.set_style("white")
    mode = "slice"
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset",default="../temporal_data/mlking", type = str)
    args = parser.parse_args()
    foldername = args.dataset
    for resolution_real in ["1h","3h","6h","12h"]:
        for downsample in [1]:
            for wsize in [1,2,3,6,12]:
                dataset = foldername.split("/")[-1]

                if "mlking" in dataset:
                    start = "2013-08-25 15:41:36"
                    end =  "2013-09-02 10:16:21"
                    
                elif "cannes" in dataset:
                    start = "2013-05-06 07:23:49"
                    end = "2013-06-03 05:48:26"
                    
                elif "moscow" in dataset:
                    start = "2013-08-05 11:25:46"
                    end =  "2013-08-19 14:35:21"

                date_range = pd.date_range(start, end, freq=resolution_real)
                resolution = len(date_range)
                print("Splitting resolution: {}".format(resolution))
                
                try:
                    print("Considering:", resolution_real,downsample,foldername)
                    int_wsize = wsize
                    temporal_frame, temporal_frame_window, partial_slices = return_entanglement_results(foldername, resolution = resolution, mode = mode, window_size = int_wsize)

                    print("Obtained the dataframe!")
                    temporal_frame = temporal_frame.groupby(['time']).mean().reset_index(drop = False)
                    pslices_series = [x.shape[0] for x in partial_slices]
                    pslices_series = [x/max(pslices_series) for x in pslices_series]
                    
                    ## do a moving one.
                    moving_volume = []
                    for enx, el in enumerate(pslices_series):
                        if enx > int_wsize:
                            wsum = np.sum(pslices_series[(enx-int_wsize):enx])
                            moving_volume.append(wsum)
                        else:
                            moving_volume.append(0)
                            
                    moving_volume = [x/max(moving_volume) for x in moving_volume]
                    
                    pslices_range = list(range(len(pslices_series)))
                    temporal_frame_window = temporal_frame_window.groupby(['time']).mean().reset_index(drop = False)

                    assert temporal_frame.shape[0] == len(pslices_series) == temporal_frame_window.shape[0]
                    drange = list(range(len(temporal_frame.Intensity)))                    
                    overall_timeframe = pd.DataFrame()
                    overall_timeframe['daterange'] = date_range
                    overall_timeframe['Intensity'] = temporal_frame.Intensity
                    overall_timeframe['NormHom'] = temporal_frame.NormHom
                    overall_timeframe['Volume'] = pslices_series
                    overall_timeframe['MovingVolume'] = moving_volume                    
                    overall_timeframe['Intensityw'] = temporal_frame_window.Intensity
                    overall_timeframe['NormHomw'] = temporal_frame_window.NormHom

                    #overall_timeframe = pd.read_csv("../time_tables/both_joint_{}_{}_{}_{}_{}.tsv".format(mode,resolution,downsample, dataset,str(wsize).replace(".","-")), sep = "\t")

                    overall_timeframe = overall_timeframe[overall_timeframe['Intensity'] > 0]
                    overall_timeframe = overall_timeframe[overall_timeframe['Intensityw'] > 0]
                    overall_timeframe = overall_timeframe[overall_timeframe['NormHom'] > 0]
                    overall_timeframe = overall_timeframe[overall_timeframe['NormHomw'] > 0]

                    plt.figure(figsize=(8, 5), dpi=300)
                    sns.lineplot(overall_timeframe.daterange, overall_timeframe.Intensity, label = "Intensity", color = "green")
                    sns.lineplot(overall_timeframe.daterange, overall_timeframe.NormHom, label = "Homogeneity", color = "orange")
                    sns.lineplot(overall_timeframe.daterange, overall_timeframe.Volume, label = "Normalized volume", color = "gray")
                    plt.xticks(rotation=90)
                    plt.legend(loc = 1)
                    plt.ylabel("Value")
                    plt.xlabel("Time period")
                    plt.tight_layout()
                    plt.savefig("../images/both_basic_{}_{}_{}_{}_{}.png".format(mode,resolution_real,downsample, dataset,str(wsize).replace(".","-")), dpi = 300)
                    plt.clf()

                    plt.figure(figsize=(8, 5), dpi=300)
                    sns.lineplot(overall_timeframe.daterange, overall_timeframe.Intensityw, label = "Intensity", color = "green")
                    sns.lineplot(overall_timeframe.daterange, overall_timeframe.NormHomw, label = "Homogeneity", color = "orange")
                    sns.lineplot(overall_timeframe.daterange, overall_timeframe.MovingVolume, label = "Moving Normalized volume", color = "gray").lines[2].set_linestyle("--")

                    sns.lineplot(overall_timeframe.daterange, overall_timeframe.Volume, label = "Normalized volume", color = "gray")
                    plt.xticks(rotation=90)
                    plt.legend(loc = 1)
                    plt.ylabel("Value")
                    plt.xlabel("Time period")
                    plt.tight_layout()
                    plt.savefig("../images/both_window_{}_{}_{}_{}_{}.png".format(mode,resolution_real,downsample, dataset,str(wsize).replace(".","-")), dpi = 300)
                    plt.clf()

                    #overall_timeframe.to_csv("../time_tables/both_joint_{}_{}_{}_{}_{}.tsv".format(mode,resolution,downsample, dataset,str(wsize).replace(".","-")), sep = "\t")

                except Exception as es:
                    ## too large res for too large downsample ..
                    print(es)
